// recordad instalar request en el proyecto

const Bicicleta = require('../../models/bicicleta')
const request = require('request')

const server = require('../../bin/www')



describe('Bicicleta API', () => {
    
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            
            const unaBici = new Bicicleta(1,'rojo','explorer',[-23213312,1231233]);
            Bicicleta.add(unaBici)

            request.get('http://localhost:3000/api/bicicletas', function( error,response,body){
                expect(response.statusCode).toBe(200)
            })
        })
    })

    describe('POST BICICLETAS /create', () => {
        it('STATUS 200', (done) => {
            
            var headers = {'content-type' : 'application/json'}
            var unaBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -45 }'
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: unaBici
            }, function(error,response,request){
                expect(response.statusCode).toBe(200)
                expect(Bicicleta.findById(10).color).toBe("rojo")
               
                done()
            })
            
           
        })
    })
})

