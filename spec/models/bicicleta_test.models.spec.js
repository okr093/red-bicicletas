const Bicicleta = require('../../models/bicicleta')

beforeEach(() => { Bicicleta.allBicis = [] })

describe('Bicicleta.allBicis', ()=>{
    it('Esta Vacio', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0)
    })
})

describe('Bicicleta.add', () => {
    it('Agregamos un elemento', () => {

            const totalActual = Bicicleta.allBicis.length
            const unaBici = new Bicicleta(1,'rojo','explorer',[-23213312,1231233]);
            Bicicleta.add(unaBici);
            expect(Bicicleta.allBicis.length).toBe(totalActual+1)    
    })
})

describe('Bicicleta.findById', () => {
    it('Encontrar un elemento por Id', () => {

        const unaBici = new Bicicleta(28,"verde","urbana",[-23213312,1231233])
        Bicicleta.add(unaBici)

        const targetBici = Bicicleta.findById(28)
        expect(targetBici.id).toBe(28)
        expect(targetBici.color).toBe(unaBici.color)
        expect(targetBici.modelo).toBe(unaBici.modelo)


    })
})

describe('Bicicleta.removeById', ( () => {
    it('remueve elemento por id', ()=>{
        const unaBici = new Bicicleta(30,"verde","urbana",[-23213312,1231233])
        //agrego una bicicleta
        Bicicleta.add(unaBici)
        expect(Bicicleta.allBicis.length).toBe(1)
        // remuevo la bicicleta agregada y comparo el total de bicicletas
        Bicicleta.removeById(30)
        expect(Bicicleta.allBicis.length).toBe(0)
     })
}))