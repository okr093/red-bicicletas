var mymap = L.map('mapid').setView([-34.5320904,-58.8151475], 14);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
   
}).addTo(mymap);

var marker = L.marker([-34.5320904,-58.8151475], 14).addTo(mymap);



$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result)
        result.bicicletas.map((bici)=>{

            L.marker(bici.ubicacion, {title: bici.modelo}).addTo(mymap)

        })
    }
})