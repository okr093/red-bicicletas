const Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function (){
    return 'id' + this.id + "| color: " + this.color;
}

Bicicleta.allBicis = [];

Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici)
}

Bicicleta.findById = (aBiciId)=>{
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId)
    if(aBici)
        return aBici;
    else 
        throw new Error('No existe  el Id proporcionado')
}

Bicicleta.removeById = (aBiciId)=>{
    

    for(var i=0; i<Bicicleta.allBicis.length; i++){

        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1)
            break
        }
    }
}

 var a = new Bicicleta(1, 'rojo', 'urbana', [-34.2320904,-58.8151475])
 var b = new Bicicleta(2, 'negro', 'explorer', [-33.921320904,-59.8151475])

 var c = new Bicicleta(231, 'rojo', 'urbana', [-34.9320904,-57.8151475])
 var d = new Bicicleta(221, 'negro', 'explorer', [-33.821320904,-57.2151475])

 Bicicleta.add(a)
 Bicicleta.add(b)
 Bicicleta.add(c)
 Bicicleta.add(d)
 module.exports = Bicicleta;